
blood_lotus_rebellion = {
	potential = {
		OR = {
			AND = {
				has_country_flag = harimari_in_uppercastes
				NOT = { has_country_flag = human_in_uppercastes }
			}
			has_country_flag = reform_the_castes
		}
		num_of_owned_provinces_with = {
			value = 60
			superregion = rahen_superregion
		}
		NOT = { has_country_flag = blood_lotus_happened }
		ai = no
	}

	
	
	can_start = {
		OR = {
			has_country_flag = reform_the_castes
			num_of_owned_provinces_with = {
				value = 100
				superregion = rahen_superregion
			}
		}
	}
	
	
	can_stop = {
        always = no
	}
	
	progress = {
		modifier = {
			factor = 1
			num_of_owned_provinces_with = {
				value = 10
				OR = {
					culture_group = upper_raheni
					culture_group = middle_raheni
					culture_group = south_raheni
					has_human_minority_trigger = yes
				}
			}
		}
		modifier = {
			factor = 1
			num_of_owned_provinces_with = {
				value = 20
				OR = {
					culture_group = upper_raheni
					culture_group = middle_raheni
					culture_group = south_raheni
					has_human_minority_trigger = yes
				}
			}
		}
		modifier = {
			factor = 1
			num_of_owned_provinces_with = {
				value = 40
				OR = {
					culture_group = upper_raheni
					culture_group = middle_raheni
					culture_group = south_raheni
					has_human_minority_trigger = yes
				}
			}
		}
		modifier = {
			factor = 1
			exists = R38
		}
	}
	
	
	
	can_end = {
		OR = {
			NOT = {
				any_owned_province = {
					OR = {
						has_province_modifier = blood_lotus_guerrillas
						has_province_modifier = blood_lotus_guerrillas_hidden
						has_province_modifier = blood_lotus_headquarter
					}
				}
				has_spawned_rebels = blood_lotus_rebel
			}
			has_country_flag = blood_lotus_lost # Rebels enforced
			# BL TODO: second option to end disaster. 
		}
	}
	
	
	modifier = {
		# tolerance_own = -2
		global_unrest = 4
		stability_cost_modifier = 1
		war_exhaustion = 0.2
	}
	
	
	on_start = blood_lotus_rebellion.1 #Spawn the initial province modifiers
	on_end = blood_lotus_rebellion.2
	
	on_monthly = {
		events = {
			blood_lotus_rebellion.101 #The Rebellion moves
			blood_lotus_rebellion.102 #The Rebellion attacks a fortress
		}
		random_events = {			
			300 = blood_lotus_rebellion.100 #The Rebellion spreads
			400 = blood_lotus_rebellion.105 #New Sympathisers appear
			300 = blood_lotus_rebellion.107 #New rebels spawn
			
			50 = blood_lotus_rebellion.4 #Rumours of a rebel force
			50 = blood_lotus_rebellion.104 #A group of guerrillas goes into hiding
			50 = blood_lotus_rebellion.103 #A rebel force moves openly
			50 = blood_lotus_rebellion.600 # General Salar Black-Claw joins the harimari hardliners. 
			100 = blood_lotus_rebellion.5 #Sympathizer negotiation
		}
	}
}

